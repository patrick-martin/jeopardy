const baseURL = 'https://jservice.kenzie.academy'
    //Getting Jeopardy Clues-- storing into an object
function getJeopardyData() {
    fetch(baseURL + '/api/clues?category=18062') //"CLASSIC CARTOON CHARACTERS"
        .then((res) => res.json())
        .then((data) => classicCartoonCharactersClues = new Object(data.clues))

    fetch(baseURL + '/api/clues?category=1519') //  'COMPUTERS
        .then((res) => res.json())
        .then((data) => computersClues = new Object(data.clues))

    fetch(baseURL + '/api/clues?category=8') // 'SPIRTED CINEMA
        .then(res => res.json())
        .then((data) => spiritedCinemaClues = new Object(data.clues))

    fetch(baseURL + '/api/clues?category=19') // 'MONTY PYTHON'
        .then(res => res.json())
        .then((data) => montyPythonClues = new Object(data.clues))

    fetch(baseURL + '/api/clues?category=26') // '90'S MUSIC'
        .then(res => res.json())
        .then((data) => ninetiesMusicClues = new Object(data.clues))

    fetch(baseURL + '/api/clues?category=34') // 'HOUSEHOLD WORDS'
        .then(res => res.json())
        .then((data) => householdWordsClues = new Object(data.clues))
}
//Call the data when the page loads

window.onload = getJeopardyData()

//Create the Jeopardy grid, give IDs, Classes, and Datasets
//DIDN'T USE GRID/CELLS FROM DEMO-- WILL DEFINITELY GO BACK AND EDIT
function roundTwoValues(index) {
    if (index < 5) { return (2 ** (index) * 100) }
    if (index === 5) return 2000
}
let main = document.querySelector('main')
for (let i = 0; i < 6; i++) {
    let row = document.createElement('div')

    row.classList.add('row')
    row.dataset.title = 'title'
    row.dataset.index = i

    main.appendChild(row)

    for (let j = 0; j < 6; j++) {
        let col = document.createElement('div')

        col.id = j
        col.classList.add('cell')
        col.dataset.x = i
        col.dataset.roundOneValue = j * 100
        col.dataset.roundTwoValue = roundTwoValues(j)

        row.appendChild(col)
    }
}

//This I will just supply from the parent element in the Grid I will use
const allCellsArray = Array.from(document.querySelectorAll('.cell'))
allCellsArray.forEach(cell => cell.addEventListener('click', showClue))

//Assign a click to each button
let answerButton = document.getElementById('answerButton')
answerButton.addEventListener('click', answerQuestion)

let passButton = document.getElementById('passButton')
passButton.addEventListener('click', passQuestion)

//Assigns the value of each clue to the cells
allCellsArray.forEach(cell => cell.innerHTML = cell.dataset.roundOneValue)

//Get the top row where we will put the titles
const elementsForCategoryTitles = allCellsArray.filter(cell => cell.id == 0)

//Titles here will be assigned to the board
//Changing a title here will change it on the board
const categoryTitles = [
        "CLASSIC CARTOON CHARACTERS",
        "COMPUTERS",
        "SPIRITED CINEMA",
        "MONTY PYTHON",
        "90'S MUSIC",
        "HOUSEHOLD WORDS",
    ]
    //Assigns the titles above to the cells in the top row
elementsForCategoryTitles
    .forEach(cell => cell.innerHTML = categoryTitles[cell.dataset.x])


//Init a single players score, 
//handle if the next clue is allowed to show, 
//store the correct answer and value of the clue the player clicked

let playerPoints = 0;
let showNextClue = 1
let lastCorrectAnswer = new String('')
let lastQuestionValue = new Number()

function changeBackgroundColor() {
    showNextClue === 1 ?
        allCellsArray.forEach(cell => cell.style.background = 'lightblue') :
        allCellsArray.forEach(cell => cell.style.background = 'salmon')
}
changeBackgroundColor()


//This is the function that will be called when we click the answer button
function answerQuestion() {
    //Get the players answer
    const myAnswer = document.getElementById('answerInput').value
        //If that answer matches the correct answer, add points, if not, subtract points
    myAnswer == lastCorrectAnswer ? playerPoints += lastQuestionValue : playerPoints -= lastQuestionValue
        //Update the players point total
    document.getElementById('myPoints').innerHTML = playerPoints


    showNextClue = 1
    changeBackgroundColor()
    document.getElementById('lastCorrectAnswer').innerHTML = lastCorrectAnswer
}

//This is the function that will be called when we click the skip button
function passQuestion() {

    showNextClue = 1
    changeBackgroundColor()
    document.getElementById('lastCorrectAnswer').innerHTML = lastCorrectAnswer
}

//This is the function that is called when we click a cell on the board
function showClue(event) {
    //If the cell is a title cell, do nothing
    if (event.path[0].dataset.roundOneValue == 0) { return void 0 } else if (showNextClue === 1) {
        //change the background color and prevent another clue from showing
        showNextClue = 0
        changeBackgroundColor()

        //Get the cell we click on, the category that cell is within, and the value of the cell
        const cell = event.path[0]
        const categoryClicked = event.path[0].parentElement.firstElementChild.innerHTML
        lastQuestionValue = event.path[0].dataset.roundOneValue

        //Creates a variable to store the exact clue in
        //in this case the clue is the first object in the '_CATEGORY_TITLE_'clues object with the value of the cell the player clicked on
        //if myClue had a value before (This is not the first clue of the game), it is reset here.
        let myClue = ''

        if (categoryClicked == "CLASSIC CARTOON CHARACTERS") { myClue = classicCartoonCharactersClues.filter(clue => clue.value == lastQuestionValue)[0] }
        if (categoryClicked == "COMPUTERS") { myClue = computersClues.filter(clue => clue.value == lastQuestionValue)[0] }
        if (categoryClicked == "SPIRITED CINEMA") { myClue = spiritedCinemaClues.filter(clue => clue.value == lastQuestionValue)[0] }
        if (categoryClicked == "MONTY PYTHON") { myClue = montyPythonClues.filter(clue => clue.value == lastQuestionValue)[0] }
        if (categoryClicked == "90'S MUSIC") { myClue = ninetiesCinemaClues.filter(clue => clue.value == lastQuestionValue)[0] }
        if (categoryClicked == "HOUSEHOLD WORDS") { myClue = householdWordsClues.filter(clue => clue.value == lastQuestionValue)[0] }

        //Display the question in the cell we clicked on
        cell.innerHTML = myClue.question
            //Store lastCorrectAnswer so we can compare it to the players answer in the answerQuestion() function
        lastCorrectAnswer = myClue.answer
    }
}